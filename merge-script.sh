#!/bin/bash
echo 'Starting the branch merges'
git clone --verbose git@gitlab.com:dsupplee/learner.git
cd ${CI_PROJECT_NAME}
# Need way of reading from config file in repo with which to determine which SIs to build/merge/etc.
git checkout test-waipio
git merge dev-waipio
git fetch https://github.com/tianocore/edk2.git master:tianocore
git merge tianocore
