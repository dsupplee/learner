#!/bin/bash
'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
eval $(ssh-agent -s)
ssh-add <(echo "$GIT_ED_PRIV_KEY")
echo $GIT_ED_PRIV_KEY
git config --global user.email "dsupplee@qti.qualcomm.com"
git config --global user.name "dsupplee"
mkdir -p ~/.ssh
ssh-keyscan gitlab.com >> gitlab-known-hosts
cat gitlab-known-hosts >> ~/.ssh/known_hosts
